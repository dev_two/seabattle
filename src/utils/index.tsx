import { difference, differenceWith, flatten } from 'lodash';

export const getRand = (min: number = 0, max: number = 99) => {
    return Math.floor(Math.random() * (max - min) + min);
}

export const getArrayOf = (size: number = 100, indexed: boolean = false) => {
    // Fun fact: for is faster than Array.from(), https://jsbench.me/mejirc5sp9/1
    const arr: number[] = [];

    for (let index = 0; index < size; index++) {
        if(indexed) { arr.push(index); continue; }
        arr.push(0);
    }

    return arr;
}

export const generatePosition = (startingPoint: number,
    length: number = 3,
    rotateBy90deg: number = 1,
    directionVertical: number = 1,
    cells: number = 10) => getArrayOf(length, true).map(item => {
        return startingPoint + (Math.pow(cells, rotateBy90deg) * item * directionVertical);
    });

export const generateLShip = () => {
    const startingPoint = getRand();
    const cells = 10;
    const pointAdjusted = (startingPoint + 1);
    const size = [0, 1, 2];
    const length = size.length - 1;

    let directionHorizontal = 1;
    let directionVertical = 1;
    let rotateBy90deg = 1;
    let points;

    if(pointAdjusted >= cells*3 && pointAdjusted <= cells*8) {
        directionVertical = Math.random() > 0.5 ? -1 : 1;
    }

    if(pointAdjusted % cells === 0) {
        directionHorizontal = -1;
    }

    if(pointAdjusted > cells*8) {
        directionVertical = -1;
    }

    if(pointAdjusted % cells > 3 && pointAdjusted % cells < 8) {
        rotateBy90deg = getRand(0, 1);
    }

    points = generatePosition(startingPoint, 3, rotateBy90deg, directionVertical, cells);
    points.push(points[length] + directionHorizontal + (rotateBy90deg === 0 ? 9 : 0));

    if(points[length+1] > 100) {
        points[length+1] = points[length+1] - cells*2
    }

    return points;
}

export const generateIShip = (otherShips: number[]) => {
    const cells = 10;

    let generateNew: boolean = true;
    let points: number[] = [];

    while (generateNew) {
        const startingPoint = getRand();
        const pointAdjusted = (startingPoint + 1);

        let directionVertical = 1;
        let rotateBy90deg = 1;
        let spaceDiff;
        let diff;

        if((startingPoint > cells * 7 && startingPoint % cells > 3) || startingPoint % cells > 5) {
            directionVertical = -1;
        }

        if(startingPoint % cells > 3 && startingPoint % cells < 7) {
            rotateBy90deg = getRand(0, 1);
        }

        if(pointAdjusted >= 70 || pointAdjusted <= 30) {
            rotateBy90deg = 0;
        }

        points = generatePosition(startingPoint, 4, rotateBy90deg, directionVertical);
        diff = difference(otherShips, points);
        spaceDiff = differenceWith(otherShips, points, (a: number, b: number) => Math.abs(a%10 - b%10) <= 1);

        if(diff.length === otherShips.length && spaceDiff.length === points.length) {
            generateNew = false;
        }
    }

    return points;
}

export const genertateDotShip = (otherShips: number[], amount: number = 2) => {
    const points: number[][] = [];

    for (let index = 0; index < amount; index++) {
        let generateNew: boolean = true;
        while (generateNew) {
            const startingPoint = getRand();

            let spaceDiff;
            let diff;

            diff = otherShips.indexOf(startingPoint) === -1; // points not overlapping
            spaceDiff = differenceWith(otherShips, [startingPoint], (a: number, b: number) => {
                const notToLeft = a - b > -1 || a - b < -2;
                const notToRight = a - b < 1 || a - b > 2;
                const notOnTop = a - b < 9 || a - b > 11;
                const notOnBottom = a - b > -9 || a - b < -11;

                return notToLeft && notToRight && notOnTop && notOnBottom;
            }); // points have distance between each other

            if(diff && spaceDiff.length === 0) {
                generateNew = false;
                points.push([startingPoint]);
                otherShips.push(startingPoint);
            }
        }
    }

    return points;
}

export const returnBattlegroundWithShips = (battleground: number[], ships: number[]) => {
    for (const position of ships) {
        battleground[position] = 1;
    }
    return battleground;
};

export const getInitialBattleground = () => {
    let battleground: number[] = getArrayOf();
    let ships: number[][] = [];
    let shipsCount = 0;

    const LShip: number[] = generateLShip();
    battleground = returnBattlegroundWithShips(battleground, LShip);

    const IShip: number[] = generateIShip(LShip);
    battleground = returnBattlegroundWithShips(battleground, IShip);

    const DotShips: number[][] = genertateDotShip([...LShip, ...IShip]);
    battleground = returnBattlegroundWithShips(battleground, flatten(DotShips));

    ships = [[...LShip], [...IShip], ...DotShips];
    shipsCount += LShip.length + IShip.length + DotShips.length;

    return { battleground, shipsCount, ships, shipsHit: 0, showShips: false };
}
