export const HIT = 'HIT';
export type HIT = typeof HIT;

export const RESET = 'RESET';
export type RESET = typeof RESET;

export const SHOW = 'SHOW';
export type SHOW = typeof SHOW;