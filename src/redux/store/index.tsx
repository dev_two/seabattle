import { createStore } from 'redux';
import { getInitialBattleground } from '../../utils';
import { BattlegroundAction } from '../actions';
import { BattlegroundReducer } from '../reducers';
import { IStoreState } from '../types';

export default createStore<IStoreState, BattlegroundAction, any, any>(BattlegroundReducer, {
    ...getInitialBattleground()
});