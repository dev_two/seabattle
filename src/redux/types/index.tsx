export interface IStoreState {
    battleground: number[];
    ships: number[][];
    shipsCount: number;
    shipsHit: number;
    showShips: boolean;
}