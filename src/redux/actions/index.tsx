import * as constants from '../constants';

export interface IHit {
    position: number
    type: constants.HIT,
}

export interface IReset {
    type: constants.RESET;
}

export interface IShowShips {
    type: constants.SHOW;
}

export type BattlegroundAction = IHit | IReset | IShowShips;

export function hit(position: number): IHit {
    return {
        position,
        type: constants.HIT
    }
}

export function reset(): IReset {
    return {
        type: constants.RESET
    }
}

export function show(): IShowShips {
    return {
        type: constants.SHOW
    }
}