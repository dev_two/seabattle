import { getInitialBattleground } from '../../utils';
import { BattlegroundAction } from '../actions';
import { HIT, RESET, SHOW } from '../constants';
import { IStoreState } from '../types';

export function BattlegroundReducer(state: IStoreState, action: BattlegroundAction): IStoreState {
  switch (action.type) {
    case HIT:
        const { battleground, shipsHit, ships } = state;
        const currentValue: number = battleground[action.position];
        let sunkCount: number = shipsHit;

        // -1 means miss,
        // 0 means empty and not clicked cell,
        // 1 means ship and not clicked,
        // 2 means sunk ship

        if(currentValue === 0 || currentValue === -1) {
          battleground[action.position] = -1;
        }
        if(currentValue === 1) {
          let currentShip: number[] = [];
          for(const ship of ships) {
            if(ship.indexOf(action.position) !== -1) {
              currentShip = ship;
            }
          }

          for(const point of currentShip) {
            battleground[point] = 2;
          }

          sunkCount = sunkCount + currentShip.length;
        }

        return {
          ...state,
          battleground: [...battleground],
          shipsHit: sunkCount
        };
    case SHOW:
      return { ...state, showShips: !state.showShips };
    case RESET:
      return { ...getInitialBattleground() };
  }
  return state;
}
