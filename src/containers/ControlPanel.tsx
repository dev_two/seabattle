import { connect, Dispatch } from 'react-redux';
import ControlPanel from '../components/ControlPanel';
import * as actions from '../redux/actions';
import { IStoreState } from '../redux/types';

function mapStateToProps({ shipsCount, shipsHit, showShips }: IStoreState) {
  return {
    shipsCount,
    shipsHit,
    showShips
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.BattlegroundAction>) {
  return {
    reset: () => dispatch(actions.reset()),
    show: () => dispatch(actions.show())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel);
