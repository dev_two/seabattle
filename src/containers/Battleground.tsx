import { connect, Dispatch } from 'react-redux';
import Battleground from '../components/Battleground';
import * as actions from '../redux/actions';
import { IStoreState } from '../redux/types';

function mapStateToProps({ showShips, battleground }: IStoreState) {
  return {
    battleground,
    showShips
  }
}

function mapDispatchToProps(dispatch: Dispatch<actions.BattlegroundAction>) {
  return {
    hit: (position: number) => dispatch(actions.hit(position))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Battleground);
