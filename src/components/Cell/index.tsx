import * as React from 'react';
import { IHit } from '../../redux/actions';
import './Cell.css';

interface ICell {
  hit: (position: number) => IHit,
  showShip: boolean,
  content: number,
  position: number
}

class Cell extends React.PureComponent<ICell> {
  
    public render() {

      return (
        <div className={this.getClassName()} onClick={this.onClick}>
          { this.props.content === -1 && '❌' }
          { this.props.content === 2  && '⛴' }
        </div>
      );
    }

    private getClassName = () => {
      const name = [];
      name.push("cell");

      if(this.props.content === 1 && this.props.showShip) {
        name.push("ship");
      }

      if(this.props.content === 2) {
        name.push("hit");
      }

      return name.join(" ");
    }

    private onClick = () => {
      if(this.props.content === 2 || this.props.content === -1) { return; }

      this.props.hit(this.props.position);
    }
}

export default Cell;