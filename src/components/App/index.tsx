import * as React from 'react';
import Battleground from '../../containers/Battleground';
import ControlPanel from '../../containers/ControlPanel';
import './App.css';
import './mobile.css';

function App() {
  return (
    <div className="App">
      <ControlPanel />
      <Battleground />
    </div>
  );
}

export default App;
