import * as React from 'react';
import { IReset, IShowShips } from '../../redux/actions';
import './ControlPanel.css';

interface IControlPanel {
  reset: () => IReset,
  show: () => IShowShips,
  shipsCount: number,
  shipsHit: number,
  showShips: boolean
}

function ControlPanel({ shipsCount, shipsHit, showShips, reset, show }: IControlPanel) {
  const areAllSunk = shipsCount === shipsHit;
  return (
    <div className="panel">
      <button className="button" onClick={reset}>Reset</button>
      <button className="button" onClick={show} disabled={areAllSunk}>Toggle ships { showShips ? 'invisible' : 'visible' }</button><br />
      <div className="victoryText">{ areAllSunk && 'All ships sunk!' }</div>
    </div>
  );
}

export default ControlPanel;