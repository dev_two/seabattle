import * as React from 'react';
import { IHit } from '../../redux/actions';
import Cell from '../Cell';
import './Battleground.css';

interface IBattleground {
  hit: (position: number) => IHit,
  battleground: number[],
  showShips: boolean
}

function Battleground({ battleground, showShips, hit }: IBattleground) {
  return (
    <div className="battleground">
      {
        battleground.map((cell, i) => (
          <Cell showShip={showShips} hit={hit} content={cell} position={i} key={i} />
        ))
      }
    </div>
  );
}

export default Battleground;